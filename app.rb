require 'sinatra' unless defined?(Sinatra)
require 'erubis'
configure do
  set :views, "#{File.dirname(__FILE__)}/views"
  SiteConfig = { :title => 'Binaries Numbers' , :author => '@filipe_mp' , :url_base =>  ENV['URL'] }
  
  $LOAD_PATH.unshift("#{File.dirname(__FILE__)}/lib")
  Dir.glob("#{File.dirname(__FILE__)}/lib/*.rb") { |lib| require File.basename(lib, '.*') }
end

error do
  e = request.env['sinatra.error']
  puts e.to_s
  puts e.backtrace.join('\n')
  'Application error'
end

get '/' do
  erb :index
end

# create
post '/generator' do
  num = params[:num].to_i
  if num.bigger_than_one
    @bin_num = BinaryNumbers.new(num) 
    erb :show
  else
    erb :try_again
  end
end
class Integer
  def bigger_than_one
    self > 1 ? true : false
  end
end

class BinaryNumbers
  attr_reader :array
  
  def initialize num
    @array =[]
    (1..num).each do |i|
      x = rand(i)
      y = rand(i*10)
      z = x * y 
      
      binary_num = "#{x.to_s(2)}*#{y.to_s(2)}=#{z.to_s(2)}\n"
      @array << binary_num
    end
  end
end